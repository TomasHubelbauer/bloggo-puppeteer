# Puppeteer

- [ ] Check out https://github.com/GoogleChrome/puppeteer/pull/2324 for ISDS
- [ ] Check on https://github.com/GoogleChrome/puppeteer/issues/299#issuecomment-389068146

[GitHub repository](https://github.com/GoogleChrome/puppeteer)

- [ ] Add some tips (file download, casting, `$$eval` etc.)

## Puppeteer API for other browsers

https://gitlab.com/TomasHubelbauer/bloggo-browser-automation
